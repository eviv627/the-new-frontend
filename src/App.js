import React from "react";
import axios from 'axios';
import Header from "./components/Header";
import Page from "./components/Page";

import "./App.css";

class App extends React.Component {
  constructor() {
    super();

    this.state = {
      loggedIn: false,
      userId: "",
      page: "Login",
    };

    this.loginHandle = this.loginHandle.bind(this);
    this.pageHandle = this.pageHandle.bind(this);
    this.getPage = this.getPage.bind(this);
    this.isLoggedIn = this.isLoggedIn.bind(this);
    this.setUpLogin = this.setUpLogin.bind(this);
  }

  setUpLogin(response) {
    this.setState({
      loggedIn: response.data.success,
      userId: response.data.value,
    });
    this.pageHandle("Project");
  }

  loginHandle(usernameReq, passwordReq) {
    axios.post(
        '/api/login',
        {
          username: usernameReq,
          password: passwordReq,
          success: false,
          value: "",
        },
        {
          headers: {
            'Content-Type': 'application/json'
          },
        }
    )
        .then(this.setUpLogin)
        .catch(function (error) {
          console.log(error);
        });

  }

  pageHandle(newPage) {
    this.setState({page: newPage});
    console.log(newPage);
  }

  getPage() {
    return(this.state.page);
  }

  isLoggedIn() {
    return(this.state.loggedIn);
  }


  render() {
    return (
      <div>
        <Header isLoggedIn={this.isLoggedIn} pageHandle={this.pageHandle} />
        <Page getPage={this.getPage} loginHandle={this.loginHandle} />
      </div>
    );
  }
}

export default App;