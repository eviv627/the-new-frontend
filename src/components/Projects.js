import React from "react";
import axios from 'axios'
import BootstrapTable from "react-bootstrap-table-next";
import cellEditFactory from 'react-bootstrap-table2-editor';

class Projects extends React.Component {
    constructor(props) {
    super(props);
    this.state = {
        isLoading: true,

        projects: [],

        columns: [
        {
          dataField: "id",
          text: "ID"
        },
        {
          dataField: "name",
          text: "Name"
        },
        {
          dataField: "description",
          text: "Description"
        },
        {
          dataField: "createDate",
          text: "Create Date",
          editable: false
        },
        {
          dataField: "startDate",
          text: "Start Date",
          editable: false
        },
        {
          dataField: "endDate",
          text: "End Date",
          editable: false
        },
        {
          dataField: "status",
          text: "Status",
          editable: false
        },
      ],
    };
  }

  componentDidMount() {
    fetch("/api/project").then(res => res.json()).then(json => {
        if(json == null) {
            json = [];
        }
        this.setState({ projects: json, isLoading: false });
    });
  }

  static projectUpdate(project) {
    axios.put(`project/`, project);
  }

  projectCreate(project) {
    axios.post(`project/`, project);
  }

  projectDelete(project) {
    axios.delete(`project/${project.id}`);
  }

  render() {
    if(this.state.isLoading) return <p>Loading...!</p>

    return (
      <BootstrapTable
        keyField="id"
        data={this.state.projects}
        columns={this.state.columns}
        cellEdit={cellEditFactory({ 
          mode: 'click',
          afterSaveCell: (oldValue, newValue, row, column) => { Projects.projectUpdate(row);},
        })}
      />
    );
  }
}

export default Projects;