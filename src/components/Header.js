import React from "react";

class Header extends React.Component {
  constructor(props) {
    super();
    this.state = {
      isLoggedIn: props.isLoggedIn,
      pageHandle: props.pageHandle
    };
  }

  render() {

    if(!this.state.isLoggedIn()) return (
      <header>
        <button
            onClick={
                () => this.state.pageHandle("Login")
                }>
            Login
        </button>
        <button
            onClick={
                () => this.state.pageHandle("Registration")
                }>
            Registration
        </button>
      </header>
    );

    return (
    <header>
        <button
            onClick={
                () => this.state.pageHandle("Projects")
                }
        >
            Projects
        </button>
        <button
            onClick={
                () => this.state.pageHandle("Tasks")
                }
        >
            Tasks
        </button>
    </header>
    );
  }
}

export default Header;