import React from "react";

import Login from "./Login";
import Projects from "./Projects";
import Tasks from "./Tasks";

class Page extends React.Component {
    constructor(props) {
        super();
        this.state = {
            loginHandle: props.loginHandle,
            getPage: props.getPage,
        };
    }

    render() {
        if (this.state.getPage() === "Projects") {
            return <Projects/>;
        }

        if (this.state.getPage() === "Tasks") {
            return <Tasks/>;
        }
        if (this.state.getPage() === "Login") {
            return <Login loginHandle={this.state.loginHandle}/>;
        }
        return "Loading....";
    }
}

export default Page;
