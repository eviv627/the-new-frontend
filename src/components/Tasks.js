import React from "react";
import axios from 'axios';
import BootstrapTable from "react-bootstrap-table-next";
import cellEditFactory from 'react-bootstrap-table2-editor';


class Tasks extends React.Component {
  constructor(props) {
    super();
    this.state = {
        isLoading: true,

        tasks: [],

        columns: [
          {
            dataField: "id",
            text: "ID"
          },
          {
            dataField: "name",
            text: "Name"
          },
          {
            dataField: "description",
            text: "Description"
          },
          {
            dataField: "createDate",
            text: "Create Date",
            editable: false
          },
          {
            dataField: "startDate",
            text: "Start Date",
            editable: false
          },
          {
            dataField: "endDate",
            text: "End Date",
            editable: false
          },
          {
            dataField: "status",
            text: "Status",
            editable: false
          },
        ],
    };
  }

  taskUpdate(task) {
    axios.put(`task/`, task);
  }

  taskCreate(task) {
    axios.post(`task/`, task);
  }

  taskDelete(task) {
    axios.delete(`task/${task.id}`);
  }

  componentDidMount() {
    fetch("/task").then(res => res.json()).then(json => {
      this.setState({ tasks: json, isLoading: false });
    });
  }

  render() {
    if(this.state.isLoading) return <p>Loading...</p>

    return (
      <BootstrapTable
        keyField="id"
        data={this.state.tasks}
        columns={this.state.columns}
        cellEdit={cellEditFactory({ 
          mode: 'click',
          afterSaveCell: (oldValue, newValue, row, column) => { this.taskUpdate(row);},
        })}
      />
    );
  }
}

export default Tasks;